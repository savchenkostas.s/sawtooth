package simplewallet.processor;

import com.google.protobuf.ByteString;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.ECKey;
import sawtooth.sdk.client.Signing;
import sawtooth.sdk.processor.Utils;
import sawtooth.sdk.protobuf.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Main {

    public static void main(String[] args) throws UnirestException, NoSuchAlgorithmException {


        ECKey privateKey = Signing.readWif(getWifKey("c1783ac6bf55910ab9bfa6defbd7b819a5b29032dd281fa1976363fd6e08d59d"));
        String publicKey = Signing.getPublicKey(privateKey);

        ByteString publicKeyByteString = ByteString.copyFromUtf8(publicKey);

        String payload = "deposit,50,02e664d454c7841ae1e2bd9a597dd12b2fa09f9389b29ab9fda6220674ff7bc7b8";
        String payloadBytes = Utils.hash512(payload.getBytes());
        ByteString payloadByteString = ByteString.copyFrom(payload.getBytes());

        TransactionHeader txnHeader = TransactionHeader.newBuilder()
                .setBatcherPublicKeyBytes(publicKeyByteString)
                .setSignerPublicKeyBytes(publicKeyByteString)
                .setFamilyName("simplewallet")
                .setFamilyVersion("1.0")
                .addInputs(publicKey)
                .setNonce("1")
                .addOutputs("02e664d454c7841ae1e2bd9a597dd12b2fa09f9389b29ab9fda6220674ff7bc7b8")
                .setPayloadSha512(payloadBytes)
                .setSignerPublicKey(publicKey)
                .build();
        ByteString txnHeaderBytes = txnHeader.toByteString();
        String txnHeaderSignature = Signing.sign(privateKey, txnHeaderBytes.toByteArray());

        Transaction txn = Transaction.newBuilder()
                .setHeader(txnHeaderBytes)
                .setPayload(payloadByteString)
                .setHeaderSignature(txnHeaderSignature)
                .build();

        BatchHeader batchHeader = BatchHeader.newBuilder()
                .setSignerPublicKey(publicKey)
                .addTransactionIds(txn.getHeaderSignature())
                .build();
        ByteString batchHeaderBytes = batchHeader.toByteString();
        String batchHeaderSignature = Signing.sign(privateKey, batchHeaderBytes.toByteArray());

        Batch batch = Batch.newBuilder()
                .setHeader(batchHeaderBytes)
                .setHeaderSignature(batchHeaderSignature)
                .addTransactions(txn)
                .build();

        BatchList batchList = BatchList.newBuilder()
                .addBatches(batch)
                .build();
        ByteString batchBytes = batchList.toByteString();

        String serverResponse = Unirest.post("http://localhost:8008/batches")
                .header("Content-Type", "application/octet-stream")
                .body(batchBytes.toByteArray())
                .asString()
                .getBody();

        System.out.println(serverResponse);

    }

    private static String getWifKey(String sourceKey) throws NoSuchAlgorithmException {

        sourceKey = "80" + sourceKey;

        byte[] pkByteArr = hexStringToByteArray(sourceKey);

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(pkByteArr);

        byte[] secondHash = digest.digest(hash);

        byte[] resPkBytes = new byte[pkByteArr.length + 4];

        System.arraycopy(pkByteArr, 0, resPkBytes, 0, pkByteArr.length);

        System.arraycopy(secondHash, 0, resPkBytes, pkByteArr.length, 4);

        return Base58.encode(resPkBytes);
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

}
